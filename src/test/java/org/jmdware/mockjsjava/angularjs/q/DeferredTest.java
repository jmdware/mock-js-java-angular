/*
 * Copyright 2015 David Ha
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jmdware.mockjsjava.angularjs.q;

import static org.hamcrest.Matchers.anEmptyMap;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.hamcrest.junit.MatcherAssert.assertThat;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.UnaryOperator;

import org.junit.Test;

public class DeferredTest {

    private static final String COUNT = "count";

    private static final int IGNORED = -1;

    private static final int ROOT_VAL = 0;

    private static final int PROMISE_1_VAL = 1;

    private static final int PROMISE_1_1_VAL = 11;

    private static final int PROMISE_1_2_VAL = 12;

    private static final int PROMISE_1_3_VAL = 13;

    private static final int PROMISE_2_VAL = 2;

    private final Deferred deferred = new Deferred();

    private final Map<String, Promise> nameToPromise = new HashMap<>(Collections.singletonMap("root", deferred.promise()));

    private final Map<String, UnaryOperator<?>> nameToDataHandler = new HashMap<>();

    private final Map<String, UnaryOperator<?>> nameToErrorHandler = new HashMap<>();

    private final Map<UnaryOperator<?>, State> handlerToState = new HashMap<>();

    @Test
    public void testResolve() {
        /*
         * root
         *     promise_1
         *         promise_1_1
         *         promise_1_2
         *             promise_1_2_1
         *             promise_1_2_2
         *         promise_1_3 (added after the ancestor root is resolved)
         *             promise_1_3_1
         *     promise_2 (added after the ancestor root is resolved)
         *         promise_2_1
         */
        givenThen("root", "promise_1", newDataHandler(ROOT_VAL, PROMISE_1_VAL));

        givenThen("promise_1", "promise_1_1", newDataHandler(PROMISE_1_VAL, PROMISE_1_1_VAL),
                newIgnoredErrorHandler());
        givenThen("promise_1", "promise_1_2", newDataHandler(PROMISE_1_VAL, PROMISE_1_2_VAL));

        givenThen("promise_1_2", "promise_1_2_1", newDataHandler(PROMISE_1_2_VAL, IGNORED),
                newIgnoredErrorHandler());

        givenThen("promise_1_2", "promise_1_2_2", newDataHandler(PROMISE_1_2_VAL, IGNORED));

        thenNoHandlersInvoked();

        whenRootResolved();

        thenPromisesResolved("promise_1", "promise_1_1", "promise_1_2", "promise_1_2_1", "promise_1_2_2");

        givenThen("promise_1", "promise_1_3", newDataHandler(PROMISE_1_VAL, PROMISE_1_3_VAL));

        thenPromisesResolved("promise_1_3");

        givenThen("promise_1_3", "promise_1_3_1", newDataHandler(PROMISE_1_3_VAL, IGNORED),
                newIgnoredErrorHandler());

        thenPromisesResolved("promise_1_3_1");

        givenThen("root", "promise_2", newDataHandler(ROOT_VAL, PROMISE_2_VAL));

        thenPromisesResolved("promise_2");

        givenThen("promise_2", "promise_2_1", newDataHandler(PROMISE_2_VAL, IGNORED),
                newIgnoredErrorHandler());

        thenPromisesResolved("promise_2_1");
    }

    @Test
    public void testReject() {
        /*
         * root
         *     promise_1
         *         promise_1_1
         *         promise_1_2
         *             promise_1_2_1
         *             promise_1_2_2
         *         promise_1_3 (added after the ancestor root is resolved)
         *             promise_1_3_1
         *     promise_2
         *         promise_2_1
         *             promise_2_1_1
         *     promise_3 (added after the ancestor root is resolved)
         *         promise_3_1
         */
        givenThen("root", "promise_1", newIgnoredDataHandler());

        givenThen("promise_1", "promise_1_1", newIgnoredDataHandler(),
                newErrorHandler(ROOT_VAL, PROMISE_1_1_VAL));
        givenThen("promise_1", "promise_1_2", newIgnoredDataHandler());

        givenThen("promise_1_2", "promise_1_2_1", newIgnoredDataHandler(),
                newErrorHandler(ROOT_VAL, IGNORED));

        givenThen("promise_1_2", "promise_1_2_2", newIgnoredDataHandler());

        givenThen("root", "promise_2", newIgnoredDataHandler(),
                newErrorHandler(ROOT_VAL, PROMISE_2_VAL));

        givenThen("promise_2", "promise_2_1", newIgnoredDataHandler(),
                newErrorHandler(PROMISE_2_VAL, IGNORED));

        givenThen("promise_2_1", "promise_2_1_1", newIgnoredDataHandler());

        thenNoHandlersInvoked();

        whenRootRejected();

        thenPromisesRejected("promise_1_1", "promise_1_2_1", "promise_2", "promise_2_1");

        givenThen("promise_1", "promise_1_3", newIgnoredDataHandler(),
                newErrorHandler(ROOT_VAL, PROMISE_1_3_VAL));
        givenThen("promise_1_3", "promise_1_3_1", newIgnoredDataHandler(),
                newErrorHandler(PROMISE_1_3_VAL, IGNORED));

        thenPromisesRejected("promise_1_3", "promise_1_3_1");

        givenThen("root", "promise_3", newIgnoredDataHandler());
        givenThen("promise_3", "promise_3_1", newIgnoredDataHandler(),
                newErrorHandler(ROOT_VAL, IGNORED));

        thenPromisesRejected("promise_3_1");
    }

    private void givenThen(String parentName, String childName, UnaryOperator<Map<String, Object>> dataHandler) {
        Promise childPromise = nameToPromise.get(parentName).then(dataHandler);

        storePromise(childName, childPromise);
        storeDataHandler(childName, dataHandler);
    }

    private void givenThen(
            String parentName,
            String childName,
            UnaryOperator<Map<String, Object>> dataHandler,
            UnaryOperator<Object> errorHandler) {
        Promise childPromise = nameToPromise.get(parentName).then(dataHandler, errorHandler);

        storePromise(childName, childPromise);
        storeDataHandler(childName, dataHandler);
        storeErrorHandler(childName, errorHandler);
    }

    private void storePromise(String name, Promise promise) {
        assertThat(nameToPromise.put(name, promise), nullValue());
    }

    private void storeDataHandler(String name, UnaryOperator<Map<String, Object>> dataHandler) {
        assertThat(nameToDataHandler.put(name, dataHandler), nullValue());
    }

    private void storeErrorHandler(String name, UnaryOperator<Object> errorHandler) {
        assertThat(nameToErrorHandler.put(name, errorHandler), nullValue());
    }

    private void whenRootResolved() {
        deferred.resolve(Collections.singletonMap(COUNT, ROOT_VAL));
    }

    private void whenRootRejected() {
        deferred.reject(ROOT_VAL);
    }

    private UnaryOperator<Map<String, Object>> newDataHandler(int expectedVal, int resolvedVal) {
        return new UnaryOperator<Map<String, Object>>() {
            @Override
            public Map<String, Object> apply(Map<String, Object> map) {
                assertThat(map, equalTo(Collections.singletonMap(COUNT, expectedVal)));

                handlerToState.put(this, State.RESOLVED);

                return Collections.singletonMap(COUNT, resolvedVal);
            }
        };
    }

    private UnaryOperator<Object> newIgnoredErrorHandler() {
        return newErrorHandler(IGNORED, IGNORED);
    }

    private UnaryOperator<Object> newErrorHandler(int expectedVal, int resolvedVal) {
        return new UnaryOperator<Object>() {
            @Override
            public Object apply(Object i) {
                assertThat(handlerToState, not(hasKey(this)));
                assertThat(i, equalTo(expectedVal));

                handlerToState.put(this, State.REJECTED);

                return resolvedVal;
            }
        };
    }

    private UnaryOperator<Map<String, Object>> newIgnoredDataHandler() {
        return newDataHandler(IGNORED, IGNORED);
    }

    private void thenPromisesResolved(String... names) {
        for (String name : names) {
            UnaryOperator<?> handler = nameToDataHandler.remove(name);

            assertThat(handler, not(nullValue()));
            assertThat(handlerToState.remove(handler), sameInstance(State.RESOLVED));
        }

        assertThat(handlerToState, anEmptyMap());
    }

    private void thenPromisesRejected(String... names) {
        for (String name : names) {
            UnaryOperator<?> handler = nameToErrorHandler.remove(name);

            assertThat(handler, not(nullValue()));
            assertThat(handlerToState.remove(handler), sameInstance(State.REJECTED));
        }

        assertThat(handlerToState, anEmptyMap());
    }

    private void thenNoHandlersInvoked() {
        assertThat(handlerToState, anEmptyMap());
    }

    private enum State {
        RESOLVED,
        REJECTED
    }
}
