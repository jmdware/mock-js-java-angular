package org.jmdware.mockjsjava.angularjs.q;

import java.util.Map;
import java.util.Objects;
import java.util.function.UnaryOperator;

/**
 * Adapts {@code jDeferred} api to the angular-js promise api.
 */
public class Promise {

    private final org.jdeferred.Promise<Map<String, Object>, Object, Object> promise;

    public Promise(org.jdeferred.Promise<Map<String, Object>, Object, Object> promise) {
        this.promise = Objects.requireNonNull(promise, "promise");
    }

    public Promise then(UnaryOperator<Map<String, Object>> dataHandler) {
        org.jdeferred.Promise<Map<String, Object>, Object, Object> newPromise = promise.then(
                dataHandler::apply);

        return new Promise(newPromise);
    }

    public Promise then(
            UnaryOperator<Map<String, Object>> dataHandler,
            UnaryOperator<Object> errorHandler) {
        org.jdeferred.Promise<Map<String, Object>, Object, Object> newPromise = promise.then(
                dataHandler::apply,
                errorHandler::apply);

        return new Promise(newPromise);
    }

    public Promise then(
            UnaryOperator<Map<String, Object>> dataHandler,
            UnaryOperator<Object> errorHandler,
            UnaryOperator<Object> notificationHandler) {
        org.jdeferred.Promise<Map<String, Object>, Object, Object> newPromise = promise.then(
                dataHandler::apply,
                errorHandler::apply,
                notificationHandler::apply);

        return new Promise(newPromise);
    }
}
