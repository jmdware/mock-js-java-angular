package org.jmdware.mockjsjava.angularjs.q;

import java.util.Map;

import org.jdeferred.impl.DeferredObject;

/**
 * Adapts {@code jDeferred} api to the angular-js deferred api.
 */
public class Deferred {

    private final org.jdeferred.Deferred<Map<String, Object>, Object, Object> deferred = new DeferredObject<>();

    public void notify(Object progress) {
        deferred.notify(progress);
    }

    public void resolve(Map<String, Object> data) {
        deferred.resolve(data);
    }

    public void reject(Object reason) {
        deferred.reject(reason);
    }

    public Promise promise() {
        return new Promise(deferred.promise());
    }
}
